<?php
/*
Plugin Name: Woocommerce Epopup
Version: 1.0
Plugin URI: http://abyxo.agency
description: Ajout de fonctionnalités diverses : minimum de commande, paiement par accompte
Author: Cédric MILLET
Author URI: http://www.cedricmillet.fr
*/

if(!defined('ABSPATH')) exit('ACCES INTERDIT.');


//====================================================================
//						INSTANCE DU PLUGIN
//====================================================================
if (class_exists('WP_WC_ABYXO_EPOPUP')) new WP_WC_ABYXO_EPOPUP;


/**
 * 
 */
class WP_WC_ABYXO_EPOPUP
{
	
	private $DEV_MODE;

	function __construct( $devmode = false )
	{
		$this->DEV_MODE = $devmode;

		$this->init_config();
		$this->init_includes();
		$this->init_hooks();
		$this->run();
	}

	//	Display Errors
	private function init_config() {
		if(  $this->DEV_MODE === true ) {
			error_reporting(E_ALL);
			ini_set('display_errors', 1);
		}
	}

	//	Ajout des fonctions core
	private function init_includes() {
		//	Classes customs dans le dossier /includes/
		$files = scandir(__DIR__.'/includes/');
		foreach ( $files as $k => $file ) {
			if( $file=='.'||$file=='..'||$file=='.htaccess' ) 	continue;
		    if( strpos($file, '.old' ) !== false) 				continue;
			require_once( __DIR__.'/includes/'.$file );
		}
	}

	//	Hooks & filters
	private function init_hooks() {
		//	CSS & JS
		add_action( 'wp_enqueue_scripts', array($this, 'enqueue_styles') );
		add_action( 'admin_enqueue_scripts', array($this, 'enqueue_styles') );
	}


	//	Instances des classes enfants
	private function run() {
		//	Accompte
		if (class_exists('WP_WC_Accompte')) 		new WP_WC_Accompte;
		//	Minimum order amount
		if (class_exists('WP_WC_Minimum_Order'))	new WP_WC_Minimum_Order;
	}

	//	CSS
	public function enqueue_styles($hook) {
		if(is_admin()) {
			//	Header CSS
			wp_enqueue_style( strtolower(__CLASS__).'__style-backend', plugin_dir_url(__FILE__) . 'assets/css/style.backend.css' );
			//	Header script
			wp_enqueue_script( strtolower(__CLASS__).'__script-backend-head', plugin_dir_url(__FILE__) . 'assets/js/script.backend.head.js' );
			//	Footer script
			wp_register_script( strtolower(__CLASS__).'__script-backend-footer', plugin_dir_url(__FILE__) . 'assets/js/script.backend.footer.js','',false,true );
    		wp_enqueue_script( strtolower(__CLASS__).'__script-backend-footer' );
		} else {
			//	Header CSS
			wp_enqueue_style( strtolower(__CLASS__).'__style-front', plugin_dir_url(__FILE__) . 'assets/css/style.front.css' );
			//	Header script
			wp_enqueue_script( strtolower(__CLASS__).'__script-front', plugin_dir_url(__FILE__) . 'assets/js/script.front.head.js' );
			//	Footer script
			wp_register_script( strtolower(__CLASS__).'__script-front-footer', plugin_dir_url(__FILE__) . 'assets/js/script.front.footer.js','',false,true );
    		wp_enqueue_script( strtolower(__CLASS__).'__script-front-footer' );
		}

		//	jQuery
		if (!wp_script_is( 'jquery', 'enqueued' ))	wp_enqueue_script( 'jquery' ); 
	}
}


//====================================================================
//						MAIN PLUGIN CLASS
//====================================================================

class WP_WC_Accompte
{
	function __construct()
	{
		$this->run();
	}

	//	Instances des classes enfants
	private function run() {
		//	Ajout onglet
		new WP_WC_Accompte_Settings_Tab();
		//	Calculateur
		new WP_WC_Accompte_Accompte_System();
	}
}

class WP_WC_Minimum_Order
{
	function __construct()
	{
		$this->run();
	}

	//	Instances des classes enfants
	private function run() {
		//	Ajout onglet
		new WP_WC_Minimum_Order_Settings_Tab();
		//	System
		new WP_WC_Minimum_Order_System();
	}
}


//====================================================================
//						ACCOMPTE
//====================================================================
class WP_WC_Accompte_Settings_Tab extends WP_WC_Accompte {
	//	https://www.speakinginbytes.com/2014/07/woocommerce-settings-tab/

    function __construct() {
    	//	Ajouter un onglet Dans Woocommerce > Options
        add_filter( 'woocommerce_settings_tabs_array', array($this, 'add_settings_tab'), 50 );
        //	Ajouter les champs dans l'onglet ajouté
        add_action( 'woocommerce_settings_tabs_settings_tab_wp_wc_accompte', array($this,'settings_tab') );
        //	Enregistrer les modifications
        add_action( 'woocommerce_update_options_settings_tab_wp_wc_accompte', array($this, 'update_settings') );
    }

    //	Ajouter un onglet
    public static function add_settings_tab( $settings_tabs ) {
        $settings_tabs['settings_tab_wp_wc_accompte'] = 'Acompte';
        return $settings_tabs;
    }

    //	Inséer le formulaire dans l'onglet
    function settings_tab() {
	    woocommerce_admin_fields( $this->get_settings() );
	}
	//	Save form fields
	function update_settings() {
	    woocommerce_update_options( $this->get_settings() );
	}

	function get_settings() {
	    $settings = array(
	        'section_title' => array(
	            'name'     => 'Acompte',
	            'type'     => 'title',
	            'desc'     => '',
	            'id'       => uniqid()
	        ),
	        '_activation' => array(
	            'name' => 'Activation',
	            'type' => 'checkbox',
	            'desc' => 'Activer / désactiver le systeme d\'acompte.',
	            'id'   => 'wp_wc_accompte_option__enable'
	        ),
	        '_pourcentage' => array(
	            'name' => 'Acompte (% du TTC)',
	            'type' => 'number',
	            'desc' => 'Entrez ici le pourcentage d\'acompte (% du TTC de la commande)',
	            'id'   => 'wp_wc_accompte_option__value'
	        ),
	        '_msg1' => array(
	            'name' => 'Message à afficher dans le panier',
	            'type' => 'textarea',
	            'desc' => 'Message à afficher dans la page /panier, vous permet d\'avertir le client qu\'il existe un reste à payer sur place.',
	            'id'   => 'wp_wc_accompte_option__cartmsg'
	        ),
	        '_msg2' => array(
	            'name' => 'Message à afficher dans la commande',
	            'type' => 'textarea',
	            'desc' => 'Message à afficher dans la page /commande, vous permet d\'avertir le client qu\'il existe un reste à payer sur place.<br>ex: "La réduction appliquée au panier correspond au reste à payer sur place, lors du retrait ou de la livraison de la commande."',
	            'id'   => 'wp_wc_accompte_option__checkoutmsg'
	        ),
	        'section_end' => array('type' => 'sectionend')
	    );
	    return  $settings ;
	}

}


class WP_WC_Accompte_Accompte_System extends WP_WC_Accompte {
	//	https://www.speakinginbytes.com/2014/07/woocommerce-settings-tab/

    function __construct() {
    	if(!$this->accompteSystemeIsEnabled())	return;
    	//	Traduire la ligne total
    	add_action('woocommerce_check_cart_items', array($this,'traduire_ligne_total_accompte'));
    	//	Appliquer la reduction
    	add_action( 'woocommerce_cart_calculate_fees', array($this, 'apply_discount'));
    	//	Ajout message
    	add_action( 'woocommerce_proceed_to_checkout', array($this, 'add_message_accompte_cart'));
    	add_action( 'woocommerce_review_order_before_submit', array($this, 'add_message_accompte_chekout'));
    }

	function traduire_ligne_total_accompte() {
	    if(is_checkout() || is_cart())
	    	add_filter('gettext', array($this, 'change_cart_totals_accompte'), 20, 3 );
	}


    
	function change_cart_totals_accompte($translated, $text, $domain){
		if($text != 'Total') return $translated;

		$translated = "Total à payer maintenant";
		return $translated;
	}


    //	Appliquer la reduction à la commande
    function apply_discount($cart) {
    	//var_dump($this->get_custom_cart_ttc($cart));
    	//	Vars
    	//$total_ttc = $this->get_custom_cart_ttc($cart);
    	//var_dump($total_ttc);
    	$total_ttc = $cart->get_cart_contents_total();
    	//$total_ttc = $cart->subtotal;
    	$pourcentage = $this->accompteSystemeGetPourcentValue();
    	$reduction = $this->getReductionToApply($total_ttc, $pourcentage);
    	//var_dump($reduction);
    	//	Check
    	if($reduction<=0)				return;
    	if($reduction>=$total_ttc)		$reduction = $total_ttc;
    	//	Negative val
    	$reduction = abs($reduction) * -1;
    	//var_dump($reduction);
    	//var_dump(abs($reduction));
    	//	Apply
    	//var_dump(floatval($reduction));
		$reduction_moins_tva = $reduction/1.2;
		$cart->add_fee("Reste à payer à la livraison / retrait de la commande", (float)$reduction);
	}

	function get_custom_cart_ttc($cart) {
		$ttc = 0;
		foreach ( $cart->get_cart() as $cart_item_key => $cart_item ) {
		    $qty = $cart_item['quantity'];
		    $price = $cart_item['data']->get_price();
		    //var_dump($cart_item);
		    $ttc += $price*$qty;
		    //var_dump($cart_item);
		    //var_dump($qty);
		}
		return round($ttc, 2);
	}

    //	Retourne true si le systeme doit appliquer un accompte
    private function accompteSystemeIsEnabled() {
    	return get_option('wp_wc_accompte_option__enable') == "yes";
    }

    //	Retourne le pourcentage de l'accompte
    private function accompteSystemeGetPourcentValue() {
    	$val = get_option('wp_wc_accompte_option__value');
    	$pourcent = intval($val);
    	if($pourcent<0) 	$pourcent = 0;
    	if($pourcent>100) 	$pourcent = 100;
    	return $pourcent;
    }

    //	Retourne le montant (float) à appliquer
    private function getReductionToApply($total_ttc, $pourcentage_accompte) {
    	$total_ttc = floatval($total_ttc);
    	if($total_ttc<=0)	return 0;
    	$pourcentage_accompte = floatval($pourcentage_accompte);

    	//	Check %
    	if($pourcentage_accompte<=0)		return 0;
    	if($pourcentage_accompte>=100)		return $total_ttc;

    	//	Calculate
    	$reduc = $total_ttc - ($pourcentage_accompte/100 * $total_ttc);

    	//	Arrondi
    	$reduc = round($reduc,2);

    	return $reduc;
    }

    //	Ajouter un message d'vertissement dans la page panier
    function add_message_accompte_cart() {
    	$msg = get_option('wp_wc_accompte_option__cartmsg');
    	if(strlen($msg)==0) return;

    	echo "<div class='wp_wc_acompte_order_msg'>";
    		echo "<p>$msg</p>";
    	echo "</div>";
    }

    //	Ajouter un message d'vertissement dans la page commande
    function add_message_accompte_chekout() {
    	$msg = get_option('wp_wc_accompte_option__checkoutmsg');
    	if(strlen($msg)==0) return;

    	echo "<div class='wp_wc_acompte_order_msg'>";
    		echo "<p>$msg</p>";
    	echo "</div>";
    }

}

//====================================================================
//						MINIMUM ORDER AMOUNT
//====================================================================

class WP_WC_Minimum_Order_Settings_Tab extends WP_WC_Minimum_Order {
	//	https://www.speakinginbytes.com/2014/07/woocommerce-settings-tab/

    function __construct() {
    	//	Ajouter un onglet Dans Woocommerce > Options
        add_filter( 'woocommerce_settings_tabs_array', array($this, 'add_settings_tab'), 50 );
        //	Ajouter les champs dans l'onglet ajouté
        add_action( 'woocommerce_settings_tabs_settings_tab_wp_wc_minimum_order_am', array($this,'settings_tab') );
        //	Enregistrer les modifications
        add_action( 'woocommerce_update_options_settings_tab_wp_wc_minimum_order_am', array($this, 'update_settings') );
    }

    //	Ajouter un onglet
    public static function add_settings_tab( $settings_tabs ) {
        $settings_tabs['settings_tab_wp_wc_minimum_order_am'] = 'Montant Minimum de commande';
        return $settings_tabs;
    }

    //	Inséer le formulaire dans l'onglet
    function settings_tab() {
	    woocommerce_admin_fields( $this->get_settings() );
	}
	//	Save form fields
	function update_settings() {
	    woocommerce_update_options( $this->get_settings() );
	}

	function get_settings() {
	    $settings = array(
	        'section_title' => array(
	            'name'     => 'Montant minimum de commande',
	            'type'     => 'title',
	            'desc'     => '',
	            'id'       => uniqid()
	        ),
	        '_activation' => array(
	            'name' => 'Activation',
	            'type' => 'checkbox',
	            'desc' => 'Activer / désactiver le minimum de commande',
	            'id'   => 'wp_wc_minorder_option__enable'
	        ),
	        '_montant' => array(
	            'name' => 'Montant minimum (€)',
	            'type' => 'number',
	            'desc' => 'Entrez ici le montant minimum de chaque commande passée sur le site',
	            'id'   => 'wp_wc_minorder_option__value'
	        ),
	        'section_end' => array('type' => 'sectionend')
	    );
	    return  $settings ;
	}

}



class WP_WC_Minimum_Order_System extends WP_WC_Minimum_Order {
	//	https://www.speakinginbytes.com/2014/07/woocommerce-settings-tab/

	private $minAmount = 99999;
	private $currentAmount = 99999;

    function __construct() {
    	if(!$this->minOrderAmountEnabled()) return;

		add_action( 'woocommerce_proceed_to_checkout', array($this, 'add_message_cart'));
		add_filter('woocommerce_order_button_html', array($this, 'disable_place_order_button_html') );
    	
    }


	
    private function minOrderAmountEnabled() {
    	return get_option('wp_wc_minorder_option__enable') == "yes";
    }

    private function getMinOrderAmount() {
    	$val = get_option('wp_wc_minorder_option__value');
    	if($val<0) $val = 0;
    	return $val;
    }

    private function getCartSubtotal() {
    	global $woocommerce;
    	$a = $woocommerce->cart->subtotal;
    	//$a = WC()->cart->total;
    	return $a;
    }

    private function minAmountIsReached() {
    	$total_ttc = $this->currentAmount;
    	$min_amount = $this->minAmount;
    	if($total_ttc<$min_amount)
    		return false;
    	return true;
    }

    //	Ajouter un message d'vertissement dans la page panier
    function add_message_cart() {

    	$this->currentAmount = $this->getCartSubtotal();
    	$this->minAmount = floatval( $this->getMinOrderAmount() );
    	if($this->minAmountIsReached()) return;
    	

		$msg = sprintf( 'Le montant minimum de commande est de %s, le total actuel est de %s.' , 
			woocommerce_price( $this->minAmount ), 
			woocommerce_price( $this->currentAmount )
		);
		//wc_add_notice( $msg, 'error');
		
		remove_action( 'woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20 );
		

		echo "<div class='wp_wc_minorder_msg'>";
    		echo "<p>$msg</p>";
    	echo "</div>";
    }

    function disable_place_order_button_html( $button ) {

    	$this->currentAmount = $this->getCartSubtotal();
    	$this->minAmount = floatval( $this->getMinOrderAmount() );
    	if($this->minAmountIsReached()) return $button;


    	$msg = sprintf( 'Le montant minimum de commande n\'est pas atteint (%s).' , 
			woocommerce_price( $this->minAmount )
		);
    	$button = "<div class='wp_wc_minorder_msg'>$msg</div>";
    	return $button;
    }
}